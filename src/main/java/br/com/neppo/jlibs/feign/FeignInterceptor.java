package br.com.neppo.jlibs.feign;

import feign.RequestInterceptor;
import feign.RequestTemplate;

public class FeignInterceptor implements RequestInterceptor {

    public FeignInterceptor() { }

    @Override
    public void apply(RequestTemplate template) {
        template.header("User-Agent", "FeignClient");
    }
}
