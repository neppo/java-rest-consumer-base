package br.com.neppo.jlibs.feign;

import java.util.ArrayList;
import java.util.List;

import feign.httpclient.ApacheHttpClient;
import org.apache.commons.lang3.StringUtils;

import br.com.neppo.exceptions.DynamicException;
import br.com.neppo.exceptions.ErrorCodes;
import feign.Feign;
import feign.RequestInterceptor;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;

public class FeignBuilder<T extends FeignClient> {

    public static interface Runnable {
        void run(Feign.Builder builder);
    }

    public static <T extends FeignClient> FeignBuilder<T> Make(String url, Class<T> target){
        return new FeignBuilder<>(url, target);
    }

    public FeignBuilder(String url, Class<T> tClass) {
        if(StringUtils.isEmpty(url) || tClass == null){
            throw new DynamicException("URL and CLASS cannot be empty.").errorCode(ErrorCodes.EMPTY);
        }
        this.tClass = tClass;
        this.url = url;
        this.security.add(new FeignInterceptor());
    }

    private Class<T> tClass = null;
    private String url = null;
    private boolean jackson = true;
    private List<RequestInterceptor> security = new ArrayList<>();
    private List<RequestInterceptor> bodyInterceptos = new ArrayList<>();
    private List<Runnable> middlewares = new ArrayList<>();

    public FeignBuilder<T> jackson(boolean bool){
        this.jackson = bool;
        return this;
    }

    public FeignBuilder<T> url(String url){
        this.url = url;
        return this;
    }

    protected Feign.Builder encoder(Feign.Builder builder){
        if(builder == null){
            return null;
        }
        builder.encoder(new JacksonEncoder());
        return builder;
    }

    protected Feign.Builder decoder(Feign.Builder builder){
        if(builder == null){
            return null;
        }
        builder.decoder(new JacksonDecoder());
        return builder;
    }

    public FeignBuilder<T> addSecurityInterceptor(RequestInterceptor interceptor){
        if(interceptor != null){
            this.security.add(interceptor);
        }
        return this;
    }

    public FeignBuilder<T> addBodyInterceptor(RequestInterceptor interceptor){
        if(interceptor != null){
            this.bodyInterceptos.add(interceptor);
        }
        return this;
    }

    /**
     * Created for the sole purpose of adding an intermediary between first call and build.
     * For more refined customization of the Feign Builder instance.
     * @param middleware a function
     * @return yours truly
     */
    public FeignBuilder<T> addMiddleware(Runnable middleware){
        if(middleware != null){
            this.middlewares.add(middleware);
        }
        return this;
    }

    private T firstBuild = null;

    /**
     * Builds T or returns the first creation of it.
     * @return
     */
    public T firstBuild(){
        if(firstBuild != null){
            return firstBuild;
        }
        return build();
    }

    public final T build(){
        Feign.Builder builder = Feign.builder().client(new ApacheHttpClient());

        // add all interceptors
        this.security.forEach(builder::requestInterceptor);
        this.bodyInterceptos.forEach(builder::requestInterceptor);

        // use jackson
        if(jackson){
            encoder(builder);
            decoder(builder);
        }

        this.middlewares.forEach(item -> {
            item.run(builder);
        });

        try{
            T output = builder.target(tClass, url);

            if(firstBuild == null){
                firstBuild = output;
            }

            return output;
        }
        catch (Exception e){
            return null;
        }
    }

    public void clear(){
        this.jackson = true;
        this.middlewares = new ArrayList<>();
        this.bodyInterceptos = new ArrayList<>();
        this.security = new ArrayList<>();
        this.url = null;
    }
}